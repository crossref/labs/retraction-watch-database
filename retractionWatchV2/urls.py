"""
URL configuration for retractionWatchV2 project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.template.defaulttags import url
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static


from rw import views
from retractionWatchV2 import settings

urlpatterns = [
    path("", views.index, name="main_page"),
    path("login/", views.login, name="login"),
    path(
        "logout/",
        auth_views.LogoutView.as_view(next_page="/"),
        name="logout",
    ),
    path("__debug__/", include("debug_toolbar.urls")),
    path("admin/", admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    path("csv/", views.csv, name="csv"),
    path("new_entry/", views.new_entry, name="new_entry"),
    path("unapproved/", views.unapproved, name="unapproved"),
    path("manage_authors/", views.manage_authors, name="manage_authors"),
    path("manage_affils/", views.manage_affils, name="manage_affils"),
    path("author/<int:author_id>/", views.author, name="author"),
    path("edit_entry/<int:update_id>/", views.edit_update, name="edit_update"),
    path("edit_author/", views.edit_author, name="edit_author"),
    path(
        "edit_author_details/<int:author_id>/",
        views.edit_author_stage_two,
        name="edit_author_stage_two",
    ),
    path("merge_author/", views.merge_author, name="merge_authors"),
    path(
        r"journal-autocomplete/",
        views.JournalAutocomplete.as_view(
            create_field="name", validate_create=True
        ),
        name="journal-autocomplete",
    ),
    path(
        r"publisher-autocomplete/",
        views.PublisherAutocomplete.as_view(
            create_field="name", validate_create=True
        ),
        name="publisher-autocomplete",
    ),
    path(
        r"author-autocomplete/",
        views.AuthorAutocomplete.as_view(),
        name="author-autocomplete",
    ),
    path(
        r"ror-autocomplete/",
        views.InstitutionAutocomplete.as_view(),
        name="ror-autocomplete",
    ),
    path(
        r"subject-autocomplete/",
        views.SubjectAutocomplete.as_view(
            create_field="name", validate_create=True
        ),
        name="subject-autocomplete",
    ),
    path(
        r"reason-autocomplete/",
        views.ReasonAutocomplete.as_view(
            create_field="name", validate_create=True
        ),
        name="reason-autocomplete",
    ),
    path(
        r"external-autocomplete/",
        views.ExternalAutocomplete.as_view(
            create_field="url", validate_create=True
        ),
        name="external-autocomplete",
    ),
    path(
        r"title-autocomplete/",
        views.TitleAutocomplete.as_view(),
        name="title-autocomplete",
    ),
    path(
        r"email-autocomplete/",
        views.EmailAutocomplete.as_view(
            create_field="email", validate_create=True
        ),
        name="email-autocomplete",
    ),
    path(
        r"author-names-autocomplete/",
        views.AuthorNamesAutocomplete.as_view(),
        name="author-names-autocomplete",
    ),
    path(
        r"institution-autocomplete/",
        views.AffiliationAutocomplete.as_view(),
        name="institution-autocomplete",
    ),
    path(
        r"article-type-autocomplete/",
        views.ArticleTypecomplete.as_view(),
        name="article-type-autocomplete",
    ),
]


if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
else:
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )
