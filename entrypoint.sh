#! /bin/bash
export PYTHONPATH="/app"
/app/manage.py collectstatic --noinput
/app/manage.py migrate

uwsgi --http 0.0.0.0:8000 \
--plugins python3 \
--protocol uwsgi \
--wsgi retractionWatchV2.wsgi:application
