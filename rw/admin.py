from django.contrib import admin

from rw import models

# Register your models here.
admin.site.register(models.Journal)
admin.site.register(models.Author)
admin.site.register(models.AuthorName)
admin.site.register(models.AuthorEmail)
admin.site.register(models.Publisher)
admin.site.register(models.RORRecord)
admin.site.register(models.Update)
admin.site.register(models.ArticleType)
admin.site.register(models.Subject)
admin.site.register(models.Reason)
admin.site.register(models.ExternalURL)
admin.site.register(models.Affiliation)
