from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, HTML
from dal import autocomplete
from django import forms
from django.utils import timezone

from rw.models import (
    Update,
    Journal,
    Publisher,
    Author,
    RORRecord,
    NATURE_CHOICES,
    ArticleType,
    Subject,
    Reason,
    ExternalURL,
    AuthorName,
    AuthorEmail,
    Affiliation,
)


class MergeAuthorForm(forms.ModelForm):
    class Meta:
        model = Author

        fields = [
            "main_name",
            "alternative_names",
            "emails",
        ]

    author = forms.models.ModelChoiceField(
        queryset=Author.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="author-autocomplete"),
    )

    author_two = forms.models.ModelChoiceField(
        queryset=Author.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="author-autocomplete"),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("author", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("author_two", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Merge"),
        )


class EditAuthorForm(forms.ModelForm):
    class Meta:
        model = Author

        fields = [
            "main_name",
            "alternative_names",
            "emails",
        ]

    author = forms.models.ModelChoiceField(
        queryset=Author.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="author-autocomplete"),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("author", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Edit >>"),
        )


class EditAuthorFormStageTwo(forms.ModelForm):
    class Meta:
        model = Author

        fields = [
            "alternative_names",
            "emails",
        ]

    alternative_names = forms.models.ModelMultipleChoiceField(
        queryset=AuthorName.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(
            url="author-names-autocomplete"
        ),
    )

    emails = forms.models.ModelMultipleChoiceField(
        queryset=AuthorEmail.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="email-autocomplete"),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column(
                    "alternative_names", css_class="form-group col-md-12 mb-0"
                ),
            ),
            Row(
                Column("emails", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Save"),
        )


class UpdateForm(forms.ModelForm):
    class Meta:
        model = Update
        fields = [
            "title",
            "approved",
            "notice_date",
            "journal_of_original",
            "journal_of_notice",
            "publisher_of_original",
            "publisher_of_notice",
            "original_date",
            "authors",
            "institutions",
            "original_paper_pubmed_id",
            "original_paper_doi",
            "original_paper_url",
            "notice_paper_pubmed_id",
            "notice_paper_doi",
            "notice_paper_url",
            "article_type",
            "nature_of_notice",
            "subjects",
            "reasons",
            "retraction_watch_urls",
            "notes",
            "admin_notes",
        ]

    title = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "Title"})
    )

    approved = forms.BooleanField(
        required=False,
    )

    journal_of_original = forms.models.ModelChoiceField(
        queryset=Journal.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="journal-autocomplete"),
    )

    journal_of_notice = forms.models.ModelChoiceField(
        queryset=Journal.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="journal-autocomplete"),
    )

    publisher_of_original = forms.models.ModelChoiceField(
        queryset=Publisher.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="publisher-autocomplete"),
    )

    publisher_of_notice = forms.models.ModelChoiceField(
        queryset=Publisher.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="publisher-autocomplete"),
    )

    subjects = forms.models.ModelMultipleChoiceField(
        queryset=Subject.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="subject-autocomplete"),
    )

    reasons = forms.models.ModelMultipleChoiceField(
        queryset=Reason.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="reason-autocomplete"),
    )

    notice_date = forms.DateField(
        widget=forms.DateInput(attrs={"type": "date"})
    )

    original_date = forms.DateField(
        widget=forms.DateInput(attrs={"type": "date"}), required=False
    )

    authors = forms.models.ModelMultipleChoiceField(
        queryset=Author.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="author-autocomplete"),
    )

    retraction_watch_urls = forms.models.ModelMultipleChoiceField(
        queryset=ExternalURL.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="external-autocomplete"),
        label="Retraction Watch URLs",
    )

    institutions = forms.models.ModelMultipleChoiceField(
        queryset=Affiliation.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(
            url="institution-autocomplete"
        ),
        label="Institutions",
    )

    original_paper_pubmed_id = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "PubMed ID"}),
        required=False,
        label="Original PubMedID",
    )

    original_paper_doi = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "DOI"}),
        required=False,
        label="Original DOI",
    )

    original_paper_url = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "URL"}),
        required=False,
        label="Original URL",
    )

    notice_paper_pubmed_id = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "PubMed ID"}),
        required=False,
        label="Notice PubMedID",
    )

    notice_paper_doi = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "DOI"}),
        required=False,
        label="Notice DOI",
    )

    notice_paper_url = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "URL"}),
        required=False,
        label="Notice URL",
    )

    article_type = forms.models.ModelMultipleChoiceField(
        queryset=ArticleType.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(
            url="article-type-autocomplete"
        ),
        label="Article Type",
    )

    nature_of_notice = forms.CharField(
        widget=forms.Select(
            attrs={"placeholder": "Nature of Notice"}, choices=NATURE_CHOICES
        ),
        required=False,
    )

    notes = forms.CharField(
        widget=forms.Textarea(attrs={"placeholder": "Notes"}),
        required=False,
    )

    admin_notes = forms.CharField(
        widget=forms.Textarea(attrs={"placeholder": "Admin Notes"}),
        required=False,
    )

    def save(self, commit=True, user=None):
        instance: Update = super(UpdateForm, self).save(commit=False)
        instance.modified_by = user
        instance.modified_on = timezone.now()

        if commit:
            self.save_m2m()
            instance.save()

        return instance

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("title", css_class="form-group col-md-11 mb-0"),
                Column(
                    "approved",
                    css_class="form-group col-md-1 mb-0 regular-select",
                ),
            ),
            Row(
                Column("authors", css_class="form-group col-md-11 mb-0"),
                HTML(
                    "<a href='{% url 'manage_authors' %}' target='_blank' "
                    "style='margin-top:2.5rem;'>Manage authors</a>"
                ),
            ),
            Row(
                Column(
                    "retraction_watch_urls",
                    css_class="form-group col-md-5 mb-0",
                ),
                Column(
                    "institutions",
                    css_class="form-group col-md-6 mb-0",
                ),
                HTML(
                    "<a href='{% url 'manage_affils' %}' target='_blank' "
                    "style='margin-top:2.5rem;'>Manage affils</a>"
                ),
            ),
            Row(
                Column(
                    "journal_of_original",
                    css_class="form-group col-md-6 mb-0",
                ),
                Column(
                    "journal_of_notice",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column(
                    "publisher_of_original",
                    css_class="form-group col-md-6 mb-0",
                ),
                Column(
                    "publisher_of_notice",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column(
                    "subjects",
                    css_class="form-group col-md-6 mb-0",
                ),
                Column(
                    "reasons",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column("original_date", css_class="form-group col-md-2 mb-0"),
                Column(
                    "original_paper_pubmed_id",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "original_paper_doi",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "original_paper_url",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "article_type",
                    css_class="form-group col-md-2 mb-0 regular-select",
                ),
            ),
            Row(
                Column("notice_date", css_class="form-group col-md-2 mb-0"),
                Column(
                    "notice_paper_pubmed_id",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "notice_paper_doi",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "notice_paper_url",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "nature_of_notice",
                    css_class="form-group col-md-2 mb-0 regular-select",
                ),
            ),
            Row(
                Column("notes", css_class="form-group col-md-6 mb-0"),
                Column("admin_notes", css_class="form-group col-md-6 mb-0"),
            ),
            Submit("submit", "Save"),
        )


class UpdateSearchFormAdmin(forms.ModelForm):
    class Meta:
        model = Update
        fields = [
            "title",
            "approved",
            "notice_date",
            "journal_of_original",
            "journal_of_notice",
            "publisher_of_original",
            "publisher_of_notice",
            "original_date",
            "authors",
            "institutions",
            "original_paper_pubmed_id",
            "original_paper_doi",
            "original_paper_url",
            "notice_paper_pubmed_id",
            "notice_paper_doi",
            "notice_paper_url",
            "article_type",
            "nature_of_notice",
            "subjects",
            "reasons",
            "retraction_watch_urls",
            "notes",
            "admin_notes",
        ]

    title = forms.models.ModelChoiceField(
        queryset=Update.objects.all(),
        widget=autocomplete.ModelSelect2(url="title-autocomplete"),
        required=False,
    )

    approved = forms.BooleanField(
        required=False,
    )

    journal_of_original = forms.models.ModelChoiceField(
        queryset=Journal.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="journal-autocomplete"),
    )

    journal_of_notice = forms.models.ModelChoiceField(
        queryset=Journal.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="journal-autocomplete"),
    )

    publisher_of_original = forms.models.ModelChoiceField(
        queryset=Publisher.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="publisher-autocomplete"),
    )

    publisher_of_notice = forms.models.ModelChoiceField(
        queryset=Publisher.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="publisher-autocomplete"),
    )

    subjects = forms.models.ModelMultipleChoiceField(
        queryset=Subject.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="subject-autocomplete"),
    )

    reasons = forms.models.ModelMultipleChoiceField(
        queryset=Reason.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="reason-autocomplete"),
    )

    notice_date = forms.DateField(
        widget=forms.DateInput(attrs={"type": "date"})
    )

    original_date = forms.DateField(
        widget=forms.DateInput(attrs={"type": "date"}), required=False
    )

    authors = forms.models.ModelMultipleChoiceField(
        queryset=Author.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="author-autocomplete"),
    )

    institutions = forms.models.ModelMultipleChoiceField(
        queryset=Affiliation.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(
            url="institution-autocomplete"
        ),
    )

    retraction_watch_urls = forms.models.ModelMultipleChoiceField(
        queryset=ExternalURL.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="external-autocomplete"),
        label="Retraction Watch URLs",
    )

    original_paper_pubmed_id = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "PubMed ID"}),
        required=False,
        label="Original PubMedID",
    )

    original_paper_doi = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "DOI"}),
        required=False,
        label="Original DOI",
    )

    original_paper_url = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "URL"}),
        required=False,
        label="Original URL",
    )

    notice_paper_pubmed_id = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "PubMed ID"}),
        required=False,
        label="Notice PubMedID",
    )

    notice_paper_doi = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "DOI"}),
        required=False,
        label="Notice DOI",
    )

    notice_paper_url = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "URL"}),
        required=False,
        label="Notice URL",
    )

    article_type = forms.ModelChoiceField(
        widget=forms.Select(attrs={"placeholder": "Article Type"}),
        queryset=ArticleType.objects.all(),
        required=False,
    )

    nature_of_notice = forms.CharField(
        widget=forms.Select(
            attrs={"placeholder": "Nature of Notice"}, choices=NATURE_CHOICES
        ),
        required=False,
    )

    notes = forms.CharField(
        widget=forms.Textarea(attrs={"placeholder": "Notes"}),
        required=False,
    )

    admin_notes = forms.CharField(
        widget=forms.Textarea(attrs={"placeholder": "Admin Notes"}),
        required=False,
    )

    def save(self, commit=True, user=None):
        instance: Update = super(UpdateForm, self).save(commit=False)
        instance.modified_by = user
        instance.modified_on = timezone.now()

        if commit:
            self.save_m2m()
            instance.save()

        return instance

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("title", css_class="form-group col-md-11 mb-0"),
                Column(
                    "approved",
                    css_class="form-group col-md-1 mb-0 regular-select",
                ),
            ),
            Row(
                Column("authors", css_class="form-group col-md-11 mb-0"),
            ),
            Row(
                Column("institutions", css_class="form-group col-md-6 mb-0"),
                Column(
                    "retraction_watch_urls",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column(
                    "journal_of_original",
                    css_class="form-group col-md-6 mb-0",
                ),
                Column(
                    "journal_of_notice",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column(
                    "publisher_of_original",
                    css_class="form-group col-md-6 mb-0",
                ),
                Column(
                    "publisher_of_notice",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column(
                    "subjects",
                    css_class="form-group col-md-6 mb-0",
                ),
                Column(
                    "reasons",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column("original_date", css_class="form-group col-md-2 mb-0"),
                Column(
                    "original_paper_pubmed_id",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "original_paper_doi",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "original_paper_url",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "article_type",
                    css_class="form-group col-md-2 mb-0 regular-select",
                ),
            ),
            Row(
                Column("notice_date", css_class="form-group col-md-2 mb-0"),
                Column(
                    "notice_paper_pubmed_id",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "notice_paper_doi",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "notice_paper_url",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "nature_of_notice",
                    css_class="form-group col-md-2 mb-0 regular-select",
                ),
            ),
            Row(
                Column("notes", css_class="form-group col-md-6 mb-0"),
                Column("admin_notes", css_class="form-group col-md-6 mb-0"),
            ),
            Submit("submit", "Save"),
        )


class UpdateSearchFormPublic(forms.ModelForm):
    class Meta:
        model = Update
        fields = [
            "title",
            "notice_date",
            "journal_of_original",
            "journal_of_notice",
            "publisher_of_original",
            "publisher_of_notice",
            "original_date",
            "authors",
            "institutions",
            "original_paper_pubmed_id",
            "original_paper_doi",
            "original_paper_url",
            "notice_paper_pubmed_id",
            "notice_paper_doi",
            "notice_paper_url",
            "article_type",
            "nature_of_notice",
            "subjects",
            "reasons",
        ]

    title = forms.models.ModelChoiceField(
        queryset=Update.objects.all(),
        widget=autocomplete.ModelSelect2(url="title-autocomplete"),
        required=False,
    )

    journal_of_original = forms.models.ModelChoiceField(
        queryset=Journal.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="journal-autocomplete"),
    )

    journal_of_notice = forms.models.ModelChoiceField(
        queryset=Journal.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="journal-autocomplete"),
    )

    publisher_of_original = forms.models.ModelChoiceField(
        queryset=Publisher.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="publisher-autocomplete"),
    )

    publisher_of_notice = forms.models.ModelChoiceField(
        queryset=Publisher.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="publisher-autocomplete"),
    )

    subjects = forms.models.ModelMultipleChoiceField(
        queryset=Subject.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="subject-autocomplete"),
    )

    reasons = forms.models.ModelMultipleChoiceField(
        queryset=Reason.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="reason-autocomplete"),
    )

    notice_date = forms.DateField(
        widget=forms.DateInput(attrs={"type": "date"}),
        required=False,
        label="Notice date from",
    )

    notice_date_to = forms.DateField(
        widget=forms.DateInput(attrs={"type": "date"}),
        required=False,
        label="Notice date to",
    )

    original_date = forms.DateField(
        widget=forms.DateInput(attrs={"type": "date"}),
        required=False,
        label="Original date from",
    )

    original_date_to = forms.DateField(
        widget=forms.DateInput(attrs={"type": "date"}),
        required=False,
        label="Original date to",
    )

    authors = forms.models.ModelMultipleChoiceField(
        queryset=Author.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(url="author-autocomplete"),
    )

    institutions = forms.models.ModelMultipleChoiceField(
        queryset=Affiliation.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(
            url="institution-autocomplete"
        ),
    )

    original_paper_pubmed_id = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "PubMed ID"}),
        required=False,
        label="Original PubMedID",
    )

    original_paper_doi = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "DOI"}),
        required=False,
        label="Original DOI",
    )

    original_paper_url = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "URL"}),
        required=False,
        label="Original URL",
    )

    notice_paper_pubmed_id = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "PubMed ID"}),
        required=False,
        label="Notice PubMedID",
    )

    notice_paper_doi = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "DOI"}),
        required=False,
        label="Notice DOI",
    )

    notice_paper_url = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "URL"}),
        required=False,
        label="Notice URL",
    )

    article_type = forms.ModelChoiceField(
        widget=forms.Select(attrs={"placeholder": "Article Type"}),
        queryset=ArticleType.objects.all(),
        required=False,
    )

    nature_of_notice = forms.CharField(
        widget=forms.Select(
            attrs={"placeholder": "Nature of Notice"}, choices=NATURE_CHOICES
        ),
        required=False,
    )

    def save(self, commit=True):
        instance: Update = super(UpdateForm, self).save(commit=False)

        return instance

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("title", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("authors", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("institutions", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column(
                    "journal_of_original",
                    css_class="form-group col-md-6 mb-0",
                ),
                Column(
                    "journal_of_notice",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column(
                    "publisher_of_original",
                    css_class="form-group col-md-6 mb-0",
                ),
                Column(
                    "publisher_of_notice",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column(
                    "subjects",
                    css_class="form-group col-md-6 mb-0",
                ),
                Column(
                    "reasons",
                    css_class="form-group col-md-6 mb-0",
                ),
            ),
            Row(
                Column("original_date", css_class="form-group col-md-2 mb-0"),
                Column(
                    "original_date_to", css_class="form-group col-md-2 mb-0"
                ),
                Column(
                    "original_paper_pubmed_id",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "original_paper_doi",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "original_paper_url",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "article_type",
                    css_class="form-group col-md-2 mb-0 regular-select",
                ),
            ),
            Row(
                Column("notice_date", css_class="form-group col-md-2 mb-0"),
                Column("notice_date_to", css_class="form-group col-md-2 mb-0"),
                Column(
                    "notice_paper_pubmed_id",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "notice_paper_doi",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "notice_paper_url",
                    css_class="form-group col-md-2 mb-0",
                ),
                Column(
                    "nature_of_notice",
                    css_class="form-group col-md-2 mb-0 regular-select",
                ),
            ),
            Submit("submit", "Search"),
        )


class NewAuthorNameForm(forms.ModelForm):
    class Meta:
        model = AuthorName
        fields = [
            "first_name",
            "last_name",
            "searchable_name",
        ]

    searchable_name = forms.CharField()

    def save(self, commit=True):
        instance: Update = super(NewAuthorNameForm, self).save(commit=False)

        if commit:
            instance.save()

        return instance

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("first_name", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("last_name", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column(
                    "searchable_name", css_class="form-group col-md-12 mb-0"
                ),
            ),
            Submit("submit", "Save New Author Name"),
        )


class NewAffiliationForm(forms.ModelForm):
    class Meta:
        model = Affiliation
        fields = [
            "department_name",
            "institution",
        ]

    department_name = forms.CharField()

    institution = forms.models.ModelChoiceField(
        queryset=RORRecord.objects.all(),
        widget=autocomplete.ModelSelect2(url="ror-autocomplete"),
        required=True,
    )

    def save(self, commit=True):
        instance: Update = super(NewAffiliationForm, self).save(commit=False)

        if commit:
            instance.save()

        return instance

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column(
                    "department_name", css_class="form-group col-md-12 mb-0"
                ),
            ),
            Row(
                Column("institution", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Save New Affiliation"),
        )


class NewRORForm(forms.ModelForm):
    class Meta:
        model = RORRecord
        fields = [
            "institution_name",
            "country",
        ]

    institution_name = forms.CharField()
    country = forms.CharField

    def save(self, commit=True):
        instance: Update = super(NewRORForm, self).save(commit=False)

        if commit:
            instance.save()

        return instance

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column(
                    "institution_name", css_class="form-group col-md-12 mb-0"
                ),
            ),
            Row(
                Column("country", css_class="form-group col-md-2 mb-0"),
            ),
            Submit("submit", "Save New Institution"),
        )


class NewAuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = [
            "emails",
        ]

    author_name = forms.models.ModelChoiceField(
        queryset=AuthorName.objects.all(),
        widget=autocomplete.ModelSelect2(url="author-names-autocomplete"),
        required=True,
    )

    emails = forms.models.ModelMultipleChoiceField(
        queryset=AuthorEmail.objects.all(),
        required=True,
        widget=autocomplete.ModelSelect2Multiple(url="email-autocomplete"),
    )

    def save(self, commit=True):
        instance: Author = Author()

        instance.main_name = self.cleaned_data["author_name"]
        instance.save()

        instance.alternative_names.clear()

        for email in self.cleaned_data["emails"]:
            instance.emails.add(email)

        if commit:
            instance.save()

        return instance

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("author_name", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("emails", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Save New Author"),
        )


class AuthorAKAForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = [
            "additional_names",
        ]

    author_object = forms.models.ModelChoiceField(
        queryset=Author.objects.all(),
        widget=autocomplete.ModelSelect2(url="author-autocomplete"),
        required=True,
    )

    additional_names = forms.models.ModelChoiceField(
        queryset=AuthorName.objects.all(),
        required=True,
        widget=autocomplete.ModelSelect2(url="author-names-autocomplete"),
    )

    def save(self, commit=True):
        instance: Author = Author.objects.get(
            id=self.cleaned_data["author_object"].id
        )

        instance.alternative_names.add(self.cleaned_data["additional_names"])
        instance.save()

        return instance

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("author_object", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column(
                    "additional_names", css_class="form-group col-md-12 mb-0"
                ),
            ),
            Submit("submit", "Save Author AKA"),
        )
