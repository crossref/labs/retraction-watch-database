from django.contrib.auth.models import User
from django.db import models
from django.db.models import ManyToManyField

NATURE_CHOICES = (
    (0, ""),
    (1, "Retraction"),
    (2, "Expression of concern"),
    (3, "Correction"),
    (4, "Reinstatement"),
)


class AuthorEmail(models.Model):
    """
    An author email associated with an update
    """

    email = models.EmailField()

    def __str__(self):
        return self.email


class AuthorName(models.Model):
    """
    An author name associated with an update
    """

    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)

    searchable_name = models.TextField()

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class RORRecord(models.Model):
    """
    A record corresponding to an ROR record
    """

    ror_id = models.URLField(blank=True, null=True)
    institution_name = models.TextField(blank=True, null=True)
    grid_id = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        indexes = [models.Index(fields=["institution_name"])]

    def __str__(self) -> str:
        return f"{self.institution_name} ({self.country})"


class Affiliation(models.Model):
    """
    An affiliation associated with an update
    """

    original_text = models.TextField(blank=True, null=True)
    institution = models.ForeignKey(
        RORRecord, on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return f"{self.original_text}"


class Author(models.Model):
    """
    An author associated with an update
    """

    main_name = models.ForeignKey(AuthorName, on_delete=models.CASCADE)

    emails = ManyToManyField(AuthorEmail, blank=True, null=True)
    alternative_names = ManyToManyField(
        AuthorName, related_name="alternative_names", blank=True, null=True
    )

    def __str__(self):
        author_name = f"{self.main_name.last_name}, {self.main_name.first_name}"

        if len(self.alternative_names.all()) > 0:
            for alt_name in self.alternative_names.all():
                if (
                    self.main_name.last_name != alt_name.last_name
                    and self.main_name.first_name != alt_name.first_name
                ):
                    author_name += (
                        f" (aka {alt_name.first_name} {alt_name.last_name})"
                    )

        for email in self.emails.all():
            author_name += f" [{email.email}]"

        return author_name


class Publisher(models.Model):
    """
    A publisher associated with an update
    """

    name = models.TextField()

    def __str__(self):
        return self.name


class Journal(models.Model):
    """
    A journal associated with an update
    """

    name = models.TextField()

    def __str__(self):
        return self.name


class ArticleType(models.Model):
    """
    An article type associated with an update
    """

    name = models.TextField()

    def __str__(self):
        return self.name


class Subject(models.Model):
    """
    A subject associated with an update
    """

    name = models.TextField()

    def __str__(self):
        return self.name


class Reason(models.Model):
    """
    A reason for a retraction or expression of concern etc
    """

    name = models.TextField()

    def __str__(self):
        return self.name


class ExternalURL(models.Model):
    """
    An external URL associated with an update
    """

    url = models.URLField()

    def __str__(self):
        return self.url


class Download(models.Model):
    download_type = models.CharField(max_length=255)
    download_data = models.TextField()


class Update(models.Model):
    """
    A retraction watch update
    """

    # mandatory fields
    title = models.TextField(blank=True)
    notice_date = models.DateField()
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    modified_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, related_name="modified_by"
    )

    created_on = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_on = models.DateTimeField(auto_now=True, blank=True, null=True)

    # optional fields
    original_date = models.DateField(null=True)
    authors = ManyToManyField(Author)
    publisher_of_original = models.ForeignKey(
        Publisher,
        on_delete=models.CASCADE,
        related_name="publisher_of_original",
        null=True,
    )
    publisher_of_notice = models.ForeignKey(
        Publisher,
        on_delete=models.CASCADE,
        related_name="publisher_of_notice",
        null=True,
    )
    journal_of_original = models.ForeignKey(
        Journal,
        on_delete=models.CASCADE,
        related_name="journal_of_original",
        null=True,
    )
    journal_of_notice = models.ForeignKey(
        Journal,
        on_delete=models.CASCADE,
        related_name="journal_of_notice",
        null=True,
    )

    original_paper_pubmed_id = models.TextField(blank=True, null=True)
    original_paper_doi = models.TextField(blank=True, null=True)
    original_paper_url = models.TextField(blank=True, null=True)

    notice_paper_pubmed_id = models.TextField(blank=True, null=True)
    notice_paper_doi = models.TextField(blank=True, null=True)
    notice_paper_url = models.TextField(blank=True, null=True)

    article_type = models.ManyToManyField(ArticleType, blank=True, null=True)

    nature_of_notice = models.IntegerField(choices=NATURE_CHOICES, null=True)

    paywalled = models.BooleanField(default=False)

    approved = models.BooleanField(default=False)

    subjects = ManyToManyField(Subject, blank=True, null=True)
    reasons = ManyToManyField(Reason, blank=True, null=True)

    retraction_watch_urls = ManyToManyField(ExternalURL)

    notes = models.TextField(null=True, blank=True)
    admin_notes = models.TextField(null=True, blank=True)

    institutions = ManyToManyField(Affiliation, blank=True, null=True)

    def __str__(self):
        return f"{self.title} ({self.notice_date} / {self.id})"
