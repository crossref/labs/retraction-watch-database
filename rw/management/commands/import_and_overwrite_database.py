import io
import json
from datetime import datetime
from datetime import timedelta
from pathlib import Path
from typing import Any

from clannotation.annotator import Annotator as Annotator
from claws import aws_utils
from django.core.management.base import BaseCommand
from django.db import transaction
from entext import entext, open_ai, ror
from unidecode import unidecode

from rw.models import (
    RORRecord,
    AuthorEmail,
    Author,
    AuthorName,
    Journal,
    Publisher,
    Update,
    ArticleType,
    Subject,
    Reason,
    ExternalURL,
    NATURE_CHOICES,
    Affiliation,
)


class Command(BaseCommand):
    """
    A management command that fetches the latest Retraction Watch data and
    overwrites the existing database
    """

    help = "Overwrites existing database with new RW data"

    @transaction.atomic
    def handle(self, *args, **options):
        # first delete all entries in all models
        _instrument_or_print(
            instrumentation=None,
            message="Deleting existing data...",
        )

        AuthorEmail.objects.all().delete()
        Author.objects.all().delete()
        AuthorName.objects.all().delete()
        Journal.objects.all().delete()
        Publisher.objects.all().delete()
        Update.objects.all().delete()
        ArticleType.objects.all().delete()
        Subject.objects.all().delete()
        Reason.objects.all().delete()
        ExternalURL.objects.all().delete()
        Affiliation.objects.all().delete()

        aws_connector = aws_utils.AWSConnector(
            bucket="outputs.research.crossref.org", unsigned=False
        )

        csv = self._get_retraction_watch(aws_connector)

        if csv is None:
            print("Error loading Retraction Watch data")
            return

        _instrument_or_print(
            instrumentation=None,
            message="Loaded Retraction Watch data",
        )

        with io.StringIO(csv) as f:
            import csv

            reader = csv.DictReader(f)
            row: dict

            limit = 1000000
            count = 0

            for row in reader:
                self._process_retraction_row(
                    aws_connector=aws_connector,
                    row=row,
                    instrumentation=None,
                )
                count += 1

                if count >= limit:
                    return

    def _process_retraction_row(
        self,
        aws_connector,
        row: dict,
        instrumentation=None,
    ) -> None:
        """
        Process a row from the CSV
        :param aws_connector: an AWSConnector instance
        :param row: the row from the CSV
        :param instrumentation: an instrumentation instance
        :return: None
        """

        # extract the DOI and its MD5
        # doi, doi_md5 = self._extract_doi(row)

        # extract fields from the CSV
        (
            nature,
            notes,
            reasons,
            retraction_doi,
            urls,
            date,
            record_id,
            title,
            subjects,
            institutions,
            journal,
            publisher,
            authors,
            rw_urls,
            article_type,
            retraction_date,
            retraction_doi,
            retraction_pubmed,
            original_date,
            original_doi,
            original_pubmed,
            paywalled,
        ) = self._extract_fields(row)

        # create the author and author name objects
        _instrument_or_print(
            instrumentation=instrumentation, message="Creating authors"
        )
        author_objects = self._create_author_objects(authors)

        # lookup or create the institution object
        _instrument_or_print(
            instrumentation=instrumentation, message="Starting ROR lookup"
        )
        institution_objects = self._create_institution_objects(institutions)

        # fetch or create reasons
        _instrument_or_print(
            instrumentation=instrumentation, message="Starting reason lookup"
        )
        reason_objects = self._create_reason_objects(reasons)

        # fetch or create subjects
        _instrument_or_print(
            instrumentation=instrumentation, message="Starting subject lookup"
        )
        subject_objects = self._create_subject_objects(subjects)

        # fetch or create article type
        _instrument_or_print(
            instrumentation=instrumentation,
            message="Starting article type lookup",
        )
        article_type_object = self._create_article_type_object(article_type)

        # fetch or create journal
        _instrument_or_print(
            instrumentation=instrumentation, message="Starting journal lookup"
        )
        journal_object = self._create_journal_object(journal)

        # fetch or create publisher
        _instrument_or_print(
            instrumentation=instrumentation, message="Starting publisher lookup"
        )
        publisher_object = self._create_publisher_object(publisher)

        # fetch or create URLs
        _instrument_or_print(
            instrumentation=instrumentation, message="Starting URL lookup"
        )
        url_objects = self._create_url_objects(urls=urls)

        for nature_result in NATURE_CHOICES:
            if nature_result[1] == nature:
                nature = nature_result[0]
                break

        # create update
        _instrument_or_print(
            instrumentation=instrumentation, message="Creating update"
        )
        update = Update.objects.create(
            title=title,
            notice_date=date,
            original_date=original_date,
            original_paper_doi=original_doi,
            original_paper_pubmed_id=original_pubmed,
            notice_paper_doi=retraction_doi,
            notice_paper_pubmed_id=retraction_pubmed,
            journal_of_notice=journal_object,
            publisher_of_notice=publisher_object,
            journal_of_original=journal_object,
            publisher_of_original=publisher_object,
            notes=notes,
            nature_of_notice=nature,
            id=record_id,
            approved=True,
            paywalled=paywalled,
        )

        # add authors to update
        for author in author_objects:
            update.authors.add(author)

        # add institutions to update
        for institution in institution_objects:
            update.institutions.add(institution)

        # add reasons to update
        for reason in reason_objects:
            update.reasons.add(reason)

        # add subjects to update
        for subject in subject_objects:
            update.subjects.add(subject)

        # add urls to update
        for url in url_objects:
            update.retraction_watch_urls.add(url)

        # add article type to update
        for article_type in article_type_object:
            update.article_type.add(article_type)

        update.save()

    @staticmethod
    def _create_url_objects(
        urls: list[str], instrumentation=None
    ) -> list[ExternalURL]:
        """
        Create the URL objects
        :param urls: the urls
        :return: the url objects
        """
        url_objects = []

        for url in urls:
            if url.strip() != "":
                _instrument_or_print(
                    instrumentation=instrumentation,
                    message=f"Looking up url {url.strip()}",
                )

                url_object, created = ExternalURL.objects.get_or_create(
                    url=url.strip(),
                )
                url_objects.append(url_object)

        return url_objects

    @staticmethod
    def _create_publisher_object(
        publisher: str, instrumentation=None
    ) -> Publisher:
        """
        Create the publisher object
        :param publisher: the publisher string
        :return: the publisher object
        """
        _instrument_or_print(
            instrumentation=instrumentation,
            message=f"Looking up publisher {publisher}",
        )

        publisher_object, created = Publisher.objects.get_or_create(
            name=publisher,
        )
        return publisher_object

    @staticmethod
    def _create_journal_object(journal: str, instrumentation=None) -> Journal:
        """
        Create the journal object
        :param journal: the journal string
        :return: the journal object
        """
        _instrument_or_print(
            instrumentation=instrumentation,
            message=f"Looking up journal {journal}",
        )

        journal_object, created = Journal.objects.get_or_create(
            name=journal,
        )
        return journal_object

    @staticmethod
    def _create_article_type_object(
        article_type: str, instrumentation=None
    ) -> ArticleType:
        """
        Create the article type object
        :param article_type: the article type
        :return: the article type object
        """
        _instrument_or_print(
            instrumentation=instrumentation,
            message=f"Looking up article type {article_type}",
        )

        article_types = []

        for article_type_split in article_type.split(";"):
            article_type_split = article_type_split.strip()

            article_type_object, created = ArticleType.objects.get_or_create(
                name=article_type_split,
            )

            article_types.append(article_type_object)

        return article_types

    @staticmethod
    def _create_subject_objects(
        subjects: list[str], instrumentation=None
    ) -> list[Subject]:
        """
        Create the subject objects
        :param subjects: the subjects
        :return: the subject objects
        """
        subject_objects = []

        for subject in subjects:
            if subject.strip() != "":
                _instrument_or_print(
                    instrumentation=instrumentation,
                    message=f"Looking up subject {subject.strip()}",
                )

                subject_object, created = Subject.objects.get_or_create(
                    name=subject.strip(),
                )
                subject_objects.append(subject_object)

        return subject_objects

    @staticmethod
    def _create_reason_objects(
        reasons: list[str], instrumentation=None
    ) -> list[Reason]:
        """
        Create the reason objects
        :param reasons: the reasons
        :return: the reason objects
        """
        reason_objects = []

        for reason in reasons:
            _instrument_or_print(
                instrumentation=instrumentation,
                message=f"Looking up reason {reason}",
            )

            reason_object, created = Reason.objects.get_or_create(
                name=reason,
            )
            reason_objects.append(reason_object)

        return reason_objects

    @staticmethod
    def _create_institution_objects(
        institutions: list[str], instrumentation=None
    ) -> list[RORRecord]:
        """
        Create the institution objects
        :param institutions: the institutions
        :return: the institution objects
        """
        institution_objects = []

        for institution in institutions:
            if institution.strip() != "":
                _instrument_or_print(
                    instrumentation=instrumentation,
                    message=f"Looking up institution {institution}",
                )

                # get or create a new affiliation object
                affiliation, created = Affiliation.objects.get_or_create(
                    original_text=institution,
                    institution=None,
                )

                institution_objects.append(affiliation)

                """

                ror_match, llm = Command._match_ror(institution)

                if ror_match is not None:
                    _instrument_or_print(
                        instrumentation=instrumentation,
                        message=f"Found ROR match for {institution}",
                    )

                    # get or create a new affiliation object
                    affiliation, created = Affiliation.objects.get_or_create(
                        original_text=institution,
                        institution=ror_match,
                    )

                    institution_objects.append(affiliation)

                    continue
                else:
                    _instrument_or_print(
                        instrumentation=instrumentation,
                        message=f"No ROR match for {institution}, creating it",
                    )

                    if "country" in llm:
                        # create a ROR object from the LLM institution name
                        ror_match, created = RORRecord.objects.get_or_create(
                            institution_name=institution,
                            country=llm["country"],
                        )
                    else:
                        # create a ROR object from the LLM institution name
                        ror_match, created = RORRecord.objects.get_or_create(
                            institution_name=institution,
                        )

                    # get or create a new affiliation object
                    affiliation, created = Affiliation.objects.get_or_create(
                        original_text=institution,
                        institution=ror_match,
                    )

                    institution_objects.append(affiliation)

                    continue
                    """

        return institution_objects

    @staticmethod
    def _create_author_objects(
        authors: list[str], instrumentation=None
    ) -> list[Author]:
        """
        Create the author and author name objects
        :param authors: the authors
        :return: the author objects
        """
        author_objects = []

        for author in authors:
            first_names = " ".join(author.split(" ")[0:-1])
            last_name = author.split(" ")[-1]
            searchable = unidecode(author)

            _instrument_or_print(
                instrumentation=instrumentation,
                message=f"Creating author {last_name}, {first_names} "
                f"({searchable})",
            )

            author_name = AuthorName.objects.create(
                first_name=first_names,
                last_name=last_name,
                searchable_name=searchable,
            )
            author_object = Author.objects.create(main_name=author_name)
            author_objects.append(author_object)

        return author_objects

    @staticmethod
    def blank_llm(term: str):
        dict = {"university": term}
        return json.dumps(dict)

    @staticmethod
    def ret_none(term: str):
        return None

    @staticmethod
    def _match_ror(institution: str) -> (RORRecord | None, str):
        result = entext.Resolver(
            llm_callable=Command.blank_llm,
            ror_callable=Command.ret_none,
        ).resolve(institution)

        institution_object = None

        if result["ror"] is not None:
            institution_object = RORRecord.objects.get(
                ror_id=result["ror"]["organization"]["id"],
            )

        return institution_object, result["llm"]

    def _extract_fields(
        self,
        row,
    ) -> tuple[
        Any,
        Any,
        list[Any],
        str | Any,
        Any,
        Any,
        Any,
        Any,
        Any,
        Any,
        Any,
        Any,
        Any,
        Any,
        Any,
        Any,
        str | Any,
        Any,
        Any,
        str | Any,
        Any,
        bool,
    ]:
        """
        Extract the fields from the CSV row
        :param row: the CSV row
        :return: the fields extracted (nature, notes, reasons, retraction_doi,
         urls, date, record_id)
        """

        title = row["Title"]
        subjects = row["Subject"].split(";")
        institutions = row["Institution"].split(";")
        journal = row["Journal"]
        publisher = row["Publisher"]
        authors = row["Author"].split(";")
        rw_urls = row["URLS"].split(";")
        article_type = row["ArticleType"]

        try:
            retraction_date = datetime.strptime(
                row["RetractionDate"], "%m/%d/%Y %H:%M"
            )
        except Exception:
            retraction_date = None

        retraction_doi = (
            row["RetractionDOI"]
            if not row["RetractionDOI"].startswith("http")
            else row["RetractionDOI"]
        )

        retraction_pubmed = row["RetractionPubMedID"]

        try:
            original_date = datetime.strptime(
                row["OriginalPaperDate"], "%m/%d/%Y %H:%M"
            )
        except Exception:
            original_date = None

        original_doi = (
            row["OriginalPaperDOI"]
            if not row["OriginalPaperDOI"].startswith("http")
            else row["OriginalPaperDOI"]
        )
        original_pubmed = row["OriginalPaperPubMedID"]

        reasons = [sub[1:] for sub in row["Reason"].split(";") if len(sub) > 0]
        nature = row["RetractionNature"]
        notes = row["Notes"]
        urls = row["URLS"].split(";")
        paywalled = row["Paywalled"] == "Yes"
        try:
            date = self._date_serial_number(int(row["RetractionDate"]))
        except ValueError:
            try:
                date = self._date_serial_number(row["RetractionDate"])
            except:
                date = None

        record_id = row["Record ID"]

        return (
            nature,
            notes,
            reasons,
            retraction_doi,
            urls,
            date,
            record_id,
            title,
            subjects,
            institutions,
            journal,
            publisher,
            authors,
            rw_urls,
            article_type,
            retraction_date,
            retraction_doi,
            retraction_pubmed,
            original_date,
            original_doi,
            original_pubmed,
            paywalled,
        )

    @staticmethod
    def _date_serial_number(serial_number: int | str) -> datetime:
        """
        Convert an Excel serial number to a Python datetime object
        :param serial_number: the date serial number
        :return: a datetime object
        """
        if type(serial_number) is str:
            try:
                return datetime.strptime(serial_number, "%m/%d/%Y 0:00")
            except ValueError:
                return datetime.strptime(
                    "01/01/1900 0:00",
                    "%m/%d/%Y 0:00",
                )

        # Excel stores dates as "number of days since 1900"
        delta = datetime(
            1899,
            12,
            30,
        ) + timedelta(days=serial_number)
        return delta

    @staticmethod
    def _extract_doi(row) -> tuple[str, str]:
        """
        Extract the DOI and its MD5 from the CSV row
        :param row: the CSV row
        :return: the DOI and its MD5
        """
        doi = row["OriginalPaperDOI"]
        doi_md5 = Annotator.doi_to_md5(doi)
        return doi, doi_md5

    @staticmethod
    def _get_retraction_watch(
        aws_connector=None, instrumentation=None
    ) -> str | None:
        """
        Download the retraction watch dump from S3
        :return: CSV string
        """
        local_file = Path(Path.home() / "RWDB.csv")

        if local_file.exists():
            _instrument_or_print(
                instrumentation=instrumentation, message="Using local data file"
            )
            return local_file.read_text(encoding="utf-8", errors="ignore")

        _instrument_or_print(
            instrumentation=instrumentation, message="No local data file found"
        )

        current_date = datetime.now()

        if aws_connector is None:
            aws_connector = aws_utils.AWSConnector(
                bucket="org.crossref.research.retractionwatch", unsigned=False
            )

        # look back over the past 30 days
        for x in range(1, 30):
            try:
                s3_path = (
                    f"uploads/RWDBDNLD{current_date.strftime('%Y%m%d')}.csv"
                )

                _instrument_or_print(
                    instrumentation=instrumentation, message=f"Trying {s3_path}"
                )

                csv = aws_connector.s3_client.get_object(
                    Bucket="org.crossref.research.retractionwatch",
                    Key=s3_path,
                )["Body"].read()

                csv_str = csv.decode("utf-8", errors="ignore")

                _instrument_or_print(
                    instrumentation=instrumentation, message=f"Loaded {s3_path}"
                )

                return csv_str
            except Exception as e:
                # take one day off current date
                current_date = current_date - timedelta(days=1)

                _instrument_or_print(
                    instrumentation=instrumentation,
                    message=f"Looking back in time... {e}",
                )

        _instrument_or_print(
            instrumentation=instrumentation, message="No data file found"
        )


def _instrument_or_print(instrumentation, message):
    if instrumentation is not None:
        instrumentation.logger.info(message)
    else:
        print(message)
