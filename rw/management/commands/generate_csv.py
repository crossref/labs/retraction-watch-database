from csv import DictWriter
from io import StringIO

from django.core.management.base import BaseCommand
from django.db import transaction
from rich.progress import track

from rw import models


class Command(BaseCommand):
    """
    A management command that generates the daily CSV entry
    """

    help = "Generate the daily CSV entry"

    @transaction.atomic
    def handle(self, *args, **options):
        fieldnames = [
            "Record ID",
            "Title",
            "Subject",
            "Institution",
            "Journal",
            "Publisher",
            "Author",
            "URLS",
            "ArticleType",
            "RetractionDate",
            "RetractionDOI",
            "RetractionPubMedID",
            "OriginalPaperDate",
            "OriginalPaperDOI",
            "OriginalPaperPubMedID",
            "RetractionNature",
            "Reason",
            "Paywalled",
            "Notes",
        ]

        csvfile = StringIO()
        writer = DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for update in track(models.Update.objects.all()):
            row = {
                "Record ID": update.id,
                "Title": update.title,
                "Subject": ";".join(
                    [subject.name for subject in update.subjects.all()]
                ),
                "Institution": ";".join(
                    [
                        affiliation.original_text
                        for affiliation in update.institutions.all()
                    ]
                ),
                "Journal": update.journal_of_notice.name,
                "Publisher": update.publisher_of_notice.name,
                "Author": ";".join(
                    [
                        author.main_name.searchable_name
                        for author in update.authors.all()
                    ]
                ),
                "URLS": ";".join(
                    [url.url for url in update.retraction_watch_urls.all()]
                ),
                "ArticleType": update.article_type,
                "RetractionDate": update.notice_date,
                "RetractionDOI": update.notice_paper_doi,
                "RetractionPubMedID": update.notice_paper_pubmed_id,
                "OriginalPaperDate": update.original_date,
                "OriginalPaperDOI": update.original_paper_doi,
                "OriginalPaperPubMedID": update.original_paper_pubmed_id,
                "RetractionNature": update.nature_of_notice,
                "Reason": ";".join(
                    [reason.name for reason in update.reasons.all()]
                ),
                "Paywalled": update.paywalled,
                "Notes": update.notes,
            }
            writer.writerow(row)

        download_object, created = models.Download.objects.get_or_create(
            download_type="csv"
        )

        download_object.download_data = csvfile.getvalue()
        download_object.save()

        print("Done")
