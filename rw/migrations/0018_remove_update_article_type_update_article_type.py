# Generated by Django 4.2.7 on 2024-01-30 14:36

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("rw", "0017_download"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="update",
            name="article_type",
        ),
        migrations.AddField(
            model_name="update",
            name="article_type",
            field=models.ManyToManyField(blank=True, null=True, to="rw.articletype"),
        ),
    ]
