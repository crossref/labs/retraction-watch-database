# Generated by Django 4.2.7 on 2023-11-07 18:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("rw", "0002_update_user"),
    ]

    operations = [
        migrations.AlterField(
            model_name="update",
            name="admin_notes",
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="update",
            name="journal_of_notice",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="journal_of_notice",
                to="rw.journal",
            ),
        ),
        migrations.AlterField(
            model_name="update",
            name="journal_of_original",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="journal_of_original",
                to="rw.journal",
            ),
        ),
        migrations.AlterField(
            model_name="update",
            name="nature_of_notice",
            field=models.IntegerField(
                choices=[
                    (1, "Retraction"),
                    (2, "Expression of Concern"),
                    (3, "Correction"),
                    (4, "Reinstatement"),
                ],
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name="update",
            name="notes",
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="update",
            name="notice_paper",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="notice_paper",
                to="rw.paperrecord",
            ),
        ),
        migrations.AlterField(
            model_name="update",
            name="original_date",
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name="update",
            name="original_paper",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="original_paper",
                to="rw.paperrecord",
            ),
        ),
        migrations.AlterField(
            model_name="update",
            name="paywalled",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="update",
            name="publisher_of_notice",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="publisher_of_notice",
                to="rw.publisher",
            ),
        ),
        migrations.AlterField(
            model_name="update",
            name="publisher_of_original",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="publisher_of_original",
                to="rw.publisher",
            ),
        ),
    ]
