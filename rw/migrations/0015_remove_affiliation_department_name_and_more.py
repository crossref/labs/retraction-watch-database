# Generated by Django 4.2.7 on 2024-01-08 14:15

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("rw", "0014_remove_author_institutions_update_institutions"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="affiliation",
            name="department_name",
        ),
        migrations.AddField(
            model_name="affiliation",
            name="original_text",
            field=models.TextField(blank=True, null=True),
        ),
    ]
