from django.template.defaulttags import register

# Custom template filter to get data from a dictionary using key in template


@register.filter
def get_item(dictionary, key):
    for item in dictionary:
        if item[0] == key:
            return item[1]
