from django.http import HttpResponseRedirect, HttpResponsePermanentRedirect
from django.shortcuts import redirect
from django.urls import reverse


def return_or_elsewhere(
    request, return_page
) -> HttpResponseRedirect | HttpResponsePermanentRedirect:
    """
    Determine whether to return to a page or go to a default page.
    :param request: a request object
    :param return_page: the default page to return to
    :return: an HttpResponseRedirect object
    """
    if "return" in request.GET:
        return redirect(request.GET["return"])
    else:
        return redirect(reverse(return_page))
