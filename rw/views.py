from io import StringIO

from dal import autocomplete
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse

from rw import models, forms

from csv import DictWriter

from rich.progress import track


@staff_member_required()
def manage_authors(request) -> HttpResponse:
    template = "frontend/manage_authors.html"

    if request.POST:
        if "Save New Author Name" in request.POST["submit"]:
            form = forms.NewAuthorNameForm(request.POST)
            if form.is_valid():
                form.save()

            new_author_form = forms.NewAuthorForm()
            author_aka_form = forms.AuthorAKAForm()
        elif "Save Author AKA" in request.POST["submit"]:
            form = forms.NewAuthorNameForm()
            new_author_form = forms.NewAuthorForm()
            author_aka_form = forms.AuthorAKAForm(request.POST)

            author_aka_form.is_valid()
            author_aka_form.save()
        else:
            form = forms.NewAuthorNameForm()
            new_author_form = forms.NewAuthorForm(request.POST)

            new_author_form.is_valid()
            new_author_form.save()

            author_aka_form = forms.AuthorAKAForm()

    else:
        form = forms.NewAuthorNameForm()
        new_author_form = forms.NewAuthorForm()
        author_aka_form = forms.AuthorAKAForm()

    context = {
        "form": form,
        "new_author_form": new_author_form,
        "author_aka_form": author_aka_form,
    }

    return render(
        request,
        template,
        context,
    )


def heartbeat(request) -> HttpResponse:
    return HttpResponse("OK")


@staff_member_required()
def edit_author_stage_two(request, author_id):
    author_object = models.Author.objects.get(id=author_id)

    if request.POST:
        form = forms.EditAuthorFormStageTwo(
            request.POST, instance=author_object
        )
        form.is_valid()
        form.save()
    else:
        form = forms.EditAuthorFormStageTwo(instance=author_object)

    template = "frontend/edit_author_stage_two.html"

    context = {
        "form": form,
        "author": author_object,
    }

    return render(
        request,
        template,
        context,
    )


@staff_member_required()
def edit_author(request):
    if request.POST:
        form = forms.EditAuthorForm(request.POST)

        form.is_valid()
        author_queryset = form.cleaned_data["author"]

        print(author_queryset.id)

        return redirect(
            reverse(
                "edit_author_stage_two",
                kwargs={"author_id": author_queryset.id},
            )
        )
    else:
        form = forms.EditAuthorForm()

    template = "frontend/edit_author.html"

    context = {
        "form": form,
    }

    return render(
        request,
        template,
        context,
    )


@staff_member_required()
def merge_author(request):
    if request.POST:
        form = forms.MergeAuthorForm(request.POST)

        form.is_valid()
        author_queryset: models.Author = form.cleaned_data["author"]
        author_two_queryset: models.Author = form.cleaned_data["author_two"]

        # get a queryset of updates that have author_two in them
        updates = models.Update.objects.filter(authors__id=author_two_queryset.id)

        # move all the updates to author one
        for update in updates:
            update.authors.remove(author_two_queryset)
            update.authors.add(author_queryset)
            update.save()

        # assign the main name of author_two as an AKA of author one
        author_queryset.alternative_names.add(author_two_queryset.main_name)

        # add all alternative names from author two to author one
        for alt_name in author_two_queryset.alternative_names.all():
            author_queryset.alternative_names.add(alt_name)

        # add all emails from author two to author one
        for email in author_two_queryset.emails.all():
            author_queryset.emails.add(email)

        # delete author two
        author_two_queryset.delete()

    else:
        form = forms.MergeAuthorForm()

    template = "frontend/merge_authors.html"

    context = {
        "form": form,
    }

    return render(
        request,
        template,
        context,
    )


@staff_member_required()
def manage_affils(request) -> HttpResponse:
    template = "frontend/manage_affils.html"

    if request.POST:
        if "Save New Affiliation" in request.POST["submit"]:
            form = forms.NewAffiliationForm(request.POST)
            if form.is_valid():
                form.save()

            new_ror_form = forms.NewRORForm()
        else:
            form = forms.NewAffiliationForm()
            new_ror_form = forms.NewRORForm(request.POST)

            if new_ror_form.is_valid():
                new_ror_form.save()

    else:
        form = forms.NewAffiliationForm()
        new_ror_form = forms.NewRORForm()

    context = {
        "form": form,
        "new_ror_form": new_ror_form,
    }

    return render(
        request,
        template,
        context,
    )


def csv(request):
    download_object, created = models.Download.objects.get_or_create(
        download_type="csv"
    )

    response = HttpResponse(
        download_object.download_data, content_type="text/csv"
    )
    response[
        "Content-Disposition"
    ] = "attachment; filename=retraction_watch.csv"
    return response


def index(request) -> HttpResponse:
    """
    The dashboard index
    :param request: the request object
    :return: an HTTP response
    """
    template = "frontend/index.html"

    if request.POST:
        form = forms.UpdateSearchFormPublic(request.POST)
        context = {"form": form}

        form.is_valid()

        # now we need to build Q queries from the form
        (
            article_type_q,
            author_q,
            institution_q,
            journal_of_notice_q,
            journal_of_original_q,
            nature_of_notice_q,
            notice_date_q,
            notice_paper_doi_q,
            notice_paper_pubmed_id_q,
            notice_paper_url_q,
            original_date_q,
            original_paper_doi_q,
            original_paper_pubmed_id_q,
            original_paper_url_q,
            publisher_of_notice_q,
            publisher_of_original_q,
            reasons_q,
            subjects_q,
            title_q,
            all_blank,
        ) = build_q_queries(form)

        if all_blank:
            updates = None
        else:
            staff_q = Q() if request.user.is_staff else Q(approved=True)

            updates = models.Update.objects.filter(
                author_q
                & institution_q
                & journal_of_original_q
                & journal_of_notice_q
                & publisher_of_original_q
                & publisher_of_notice_q
                & subjects_q
                & reasons_q
                & original_date_q
                & notice_date_q
                & original_paper_pubmed_id_q
                & original_paper_doi_q
                & original_paper_url_q
                & article_type_q
                & notice_paper_pubmed_id_q
                & notice_paper_doi_q
                & notice_paper_url_q
                & nature_of_notice_q
                & staff_q
                & title_q
            )

            # That was a lot of Q.
            # Goodbye, Jean-Luc. I'm gonna miss you. You had such potential.
            # But then again, all good things must come to an end.

        context["updates"] = updates

    else:
        form = forms.UpdateSearchFormPublic()
        context = {"form": form}

    context["natures"] = models.NATURE_CHOICES

    return render(
        request,
        template,
        context,
    )


def build_q_queries(form):
    all_blank = True

    author_queryset = form.cleaned_data["authors"]
    author_q = (
        Q(authors__in=author_queryset) if len(author_queryset) > 0 else Q()
    )

    if author_q != Q():
        all_blank = False

    institution_queryset = form.cleaned_data["institutions"]
    institution_q = (
        Q(institutions__in=institution_queryset)
        if len(institution_queryset) > 0
        else Q()
    )

    if institution_q != Q():
        all_blank = False

    journal_of_original = form.cleaned_data["journal_of_original"]
    journal_of_original_q = (
        Q(journal_of_original=journal_of_original)
        if journal_of_original is not None
        else Q()
    )

    if journal_of_original_q != Q():
        all_blank = False

    journal_of_notice = form.cleaned_data["journal_of_notice"]
    journal_of_notice_q = (
        Q(journal_of_notice=journal_of_notice)
        if journal_of_notice is not None
        else Q()
    )

    if journal_of_notice_q != Q():
        all_blank = False

    publisher_of_original = form.cleaned_data["publisher_of_original"]
    publisher_of_original_q = (
        Q(publisher_of_original=publisher_of_original)
        if publisher_of_original is not None
        else Q()
    )

    if publisher_of_original_q != Q():
        all_blank = False

    publisher_of_notice = form.cleaned_data["publisher_of_notice"]
    publisher_of_notice_q = (
        Q(publisher_of_notice=publisher_of_notice)
        if publisher_of_notice is not None
        else Q()
    )

    if publisher_of_notice_q != Q():
        all_blank = False

    subjects = form.cleaned_data["subjects"]
    subjects_q = Q(subjects__in=subjects) if len(subjects) > 0 else Q()

    if subjects_q != Q():
        all_blank = False

    reasons = form.cleaned_data["reasons"]
    reasons_q = Q(reasons__in=reasons) if len(reasons) > 0 else Q()

    if reasons_q != Q():
        all_blank = False

    original_date = form.cleaned_data["original_date"]
    original_date_to = form.cleaned_data["original_date_to"]
    original_date_q = (
        Q(original_date__range=(original_date, original_date_to))
        if original_date is not None and original_date_to is not None
        else Q()
    )

    if original_date_q != Q():
        all_blank = False

    notice_date = form.cleaned_data["notice_date"]
    notice_date_to = form.cleaned_data["notice_date_to"]
    notice_date_q = (
        Q(notice_date__range=(notice_date, notice_date_to))
        if notice_date is not None and notice_date_to is not None
        else Q()
    )

    if notice_date_q != Q():
        all_blank = False

    original_paper_pubmed_id = form.cleaned_data["original_paper_pubmed_id"]
    original_paper_pubmed_id_q = (
        Q(original_paper_pubmed_id__icontains=original_paper_pubmed_id)
        if original_paper_pubmed_id != ""
        else Q()
    )

    if original_paper_pubmed_id_q != Q():
        all_blank = False

    original_paper_doi = form.cleaned_data["original_paper_doi"]
    original_paper_doi_q = (
        Q(original_paper_doi__icontains=original_paper_doi)
        if original_paper_doi != ""
        else Q()
    )

    if original_paper_doi_q != Q():
        all_blank = False

    original_paper_url = form.cleaned_data["original_paper_url"]
    original_paper_url_q = (
        Q(original_paper_url__icontains=original_paper_url)
        if original_paper_url != ""
        else Q()
    )

    if original_paper_url_q != Q():
        all_blank = False

    article_type = form.cleaned_data["article_type"]
    article_type_q = (
        Q(article_type=article_type) if article_type is not None else Q()
    )

    if article_type_q != Q():
        all_blank = False

    notice_paper_pubmed_id = form.cleaned_data["notice_paper_pubmed_id"]
    notice_paper_pubmed_id_q = (
        Q(notice_paper_pubmed_id__icontains=notice_paper_pubmed_id)
        if notice_paper_pubmed_id != ""
        else Q()
    )

    if notice_paper_pubmed_id_q != Q():
        all_blank = False

    notice_paper_doi = form.cleaned_data["notice_paper_doi"]
    notice_paper_doi_q = (
        Q(notice_paper_doi__icontains=notice_paper_doi)
        if notice_paper_doi != ""
        else Q()
    )

    if notice_paper_doi_q != Q():
        all_blank = False

    notice_paper_url = form.cleaned_data["notice_paper_url"]
    notice_paper_url_q = (
        Q(notice_paper_url__icontains=notice_paper_url)
        if notice_paper_url != ""
        else Q()
    )

    if notice_paper_url_q != Q():
        all_blank = False

    nature_of_notice = form.cleaned_data["nature_of_notice"]
    nature_of_notice_q = (
        Q(nature_of_notice=nature_of_notice)
        if nature_of_notice is not None and nature_of_notice != "0"
        else Q()
    )

    if nature_of_notice_q != Q():
        all_blank = False

    title_q = (
        Q(id=form.cleaned_data["title"].id)
        if form.cleaned_data["title"] is not None
        else Q()
    )

    if title_q != Q():
        all_blank = False

    return (
        article_type_q,
        author_q,
        institution_q,
        journal_of_notice_q,
        journal_of_original_q,
        nature_of_notice_q,
        notice_date_q,
        notice_paper_doi_q,
        notice_paper_pubmed_id_q,
        notice_paper_url_q,
        original_date_q,
        original_paper_doi_q,
        original_paper_pubmed_id_q,
        original_paper_url_q,
        publisher_of_notice_q,
        publisher_of_original_q,
        reasons_q,
        subjects_q,
        title_q,
        all_blank,
    )


@staff_member_required
def new_entry(request) -> HttpResponse:
    """
    The dashboard index
    :param request: the request object
    :return: an HTTP response
    """

    form = {}
    if request.POST:
        title = request.POST.get("title", None)
        date_of_notice = request.POST.get("date_of_notice", None)

        if title is None or title == "":
            form["title"] = {"errors": "Title is required"}

        if date_of_notice is None or date_of_notice == "":
            form["date_of_notice"] = {"errors": "Date of notice is required"}

        if len(form.keys()) == 0:
            # no errors
            new_update = models.Update()
            new_update.title = title
            new_update.notice_date = date_of_notice
            new_update.user = request.user
            new_update.save()

            return redirect(
                reverse("edit_update", kwargs={"update_id": new_update.id})
            )

    template = "frontend/new_update.html"

    context = {"form": form}

    return render(
        request,
        template,
        context,
    )


@login_required
def login(request) -> HttpResponse:
    return index(request)


def author(request, author_id) -> HttpResponse:
    """
    The author page
    :param request: the request object
    :param author_id: the ID of the author to view
    :return: an HTTP response
    """
    author_object = models.Author.objects.get(id=author_id)
    updates = models.Update.objects.filter(authors__id=author_object.id)

    context = {"updates": updates, "author": author_object}

    template = "frontend/author.html"

    return render(
        request,
        template,
        context,
    )


@staff_member_required()
def unapproved(request) -> HttpResponse:
    """
    The root index that shows unapproved items
    :param request: the request object
    :return: an HTTP response
    """
    updates = models.Update.objects.filter(approved=False)

    context = {
        "updates": updates,
    }

    template = "frontend/quality_control.html"

    return render(
        request,
        template,
        context,
    )


@staff_member_required()
def edit_update(request, update_id) -> HttpResponse:
    """
    The dashboard index
    :param request: the request object
    :param update_id: the ID of the update to edit
    :return: an HTTP response
    """
    update = models.Update.objects.get(id=update_id)

    if request.POST:
        form = forms.UpdateForm(request.POST, instance=update)
        form.save(commit=True, user=request.user)

        if "+ Journal" in request.POST["submit"]:
            print("+Jrnl")
        elif "+ Pblshr" in request.POST["submit"]:
            print("+Pub")

    template = "frontend/edit_update.html"

    form = forms.UpdateForm(instance=update)

    context = {"form": form, "update": update}

    return render(
        request,
        template,
        context,
    )


class JournalAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Journal.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs


class PublisherAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Publisher.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs


class AuthorAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Author.objects.all()

        if self.q:
            qs = qs.filter(
                Q(main_name__first_name__icontains=self.q)
                | Q(main_name__last_name__icontains=self.q)
                | Q(emails__email__icontains=self.q)
                | Q(alternative_names__first_name__icontains=self.q)
                | Q(alternative_names__last_name__icontains=self.q)
                | Q(main_name__searchable_name__icontains=self.q)
                | Q(alternative_names__searchable_name__icontains=self.q)
            ).distinct()

        return qs


class AuthorNamesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.AuthorName.objects.all()

        if self.q:
            qs = qs.filter(
                Q(first_name__icontains=self.q)
                | Q(last_name__icontains=self.q)
                | Q(searchable_name__icontains=self.q)
            ).distinct()

        return qs


class AffiliationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Affiliation.objects.all()

        if self.q:
            qs = qs.filter(Q(original_text__icontains=self.q))

        return qs


class InstitutionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.RORRecord.objects.all()

        if self.q:
            qs = qs.filter(
                Q(institution_name__icontains=self.q)
                | Q(ror_id__icontains=self.q)
            ).distinct()

        return qs


class SubjectAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Subject.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q).distinct()

        return qs


class ReasonAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Reason.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs


class ExternalAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.ExternalURL.objects.all()

        if self.q:
            qs = qs.filter(url__icontains=self.q)

        return qs


class TitleAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Update.objects.all()

        if self.q:
            qs = qs.filter(title__icontains=self.q)

        return qs


class EmailAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.AuthorEmail.objects.all()

        if self.q:
            qs = qs.filter(email__icontains=self.q).distinct()

        return qs


class ArticleTypecomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.ArticleType.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q).distinct()

        return qs
