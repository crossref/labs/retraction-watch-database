$(document).ready(function() {

    document.querySelector("#id_journal_of_original").onchange = function () {
        matl_sel = $(this).find("option:selected").text();
    }

    document.querySelector("#id_original_date").onchange = function () {
        check_dates();
    }

    document.querySelector("#id_notice_date").onchange = function () {
        check_dates();
    }

    function check_dates() {
        let ms = Date.parse($("#id_original_date").val());
        let notice_ms = Date.parse($("#id_notice_date").val());

        if (ms > notice_ms) {
            alert("Original date must be earlier than notice date");
        }
    }
});